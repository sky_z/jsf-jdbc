package com.ko.jsf.jdbc;

import javax.faces.bean.ManagedBean;

@ManagedBean
public class Product {
	
	// fields
	private int id;
	private String name;
	private String category;
	private String code;
	
	// no-arg constructor
	public Product() {
		
	}

	// constructor with fields
	public Product(int id, String name, String category, String code) {
		this.id = id;
		this.name = name;
		this.category = category;
		this.code = code;
	}

	// getters and setters
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getCategory() {
		return category;
	}

	public void setCategory(String category) {
		this.category = category;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	@Override
	public String toString() {
		return "Product [id=" + id + ", name=" + name + ", category=" + category + ", code=" + code + "]";
	}
	
}
